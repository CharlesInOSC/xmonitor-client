#!/bin/bash
version="1.0.2"
rm xmonitor-client_${version}.tar.gz
rm dist/README.TXT
cp README.md dist/
cp CHANGELOG dist/
cp LICENSE dist/
cd dist
tar zcfv ../xmonitor-client_${version}.tar.gz ./
